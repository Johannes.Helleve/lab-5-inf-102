package INF102.lab5.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private List<V> found;

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.found = new ArrayList<>();
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        found.clear();

        letsGo(u);
        if(found.contains(v)){
            return true;
        }else{
            return false;
        }
    }

    public void letsGo(V u){
        found.add(u);
        if(graph.getNeighbourhood(u) != null){
            Set<V> neighbours = graph.getNeighbourhood(u);
            for(V x : neighbours){
                if(found.contains(x)){
                    continue;
                }else{
                    letsGo(x);
                }
            }
        };
    }
}
