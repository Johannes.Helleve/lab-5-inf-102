package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        if(!nodes.contains(node)){
            nodes.add(node);
            nodeToNode.put(node, new HashSet<>());
        }
    }

    @Override
    public void removeNode(V node) {
        Set<V> connected = getNeighbourhood(node);
        for(V v : connected){
            Set<V> oldSet = getNeighbourhood(v);
            Set<V> newSet = new HashSet<>(oldSet);
            newSet.remove(node);
            nodeToNode.put(node, newSet); 
        }
        nodes.remove(node);
        nodeToNode.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        if(nodes.contains(v) && nodes.contains(u)){
            if(getNeighbourhood(u) != null){
                Set<V> oldSet = getNeighbourhood(u);
                Set<V> newSet = new HashSet<>(oldSet);
                newSet.add(v);
                nodeToNode.put(u, newSet);
            }else{
                throw new NullPointerException("Has no neighbour nodes :(");
            }
            if(getNeighbourhood(v) != null){
                Set<V> oldSet2 = getNeighbourhood(v);
                Set<V> newSet2 = new HashSet<>(oldSet2);
                newSet2.add(u);
                nodeToNode.put(v, newSet2);
            }else{
                throw new NullPointerException("Has no neighbour nodes :(");
            }
            
        }else{
            throw new IllegalArgumentException("Cant add edge to node not in graph");
        }
    }

    @Override
    public void removeEdge(V u, V v) {
         if(nodes.contains(v) && nodes.contains(u)){
            if(getNeighbourhood(u) != null){
                Set<V> oldSet = getNeighbourhood(u);
                Set<V> newSet = new HashSet<>(oldSet);
                newSet.remove(v);
                nodeToNode.put(u, newSet);
            }else{
                throw new NullPointerException("Has no neighbour nodes :(");
            }
            if(getNeighbourhood(v) != null){
                Set<V> oldSet2 = getNeighbourhood(v);
                Set<V> newSet2 = new HashSet<>(oldSet2);
                newSet2.remove(u);
                nodeToNode.put(v, newSet2);
            }else{
                throw new NullPointerException("Has no neighbour nodes :(");
            }
            
        }else{
            throw new NullPointerException();
        }
    }

    @Override
    public boolean hasNode(V node) {
        return nodes.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
